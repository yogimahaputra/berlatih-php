<?php
    require 'frog.php';
    require 'ape.php';

    $sheep = new Animal("shaun");
    echo "Name : ".$sheep->name."<br>";
    echo "legs : ".$sheep->legs."<br>";
    echo "cold blooded : ".$sheep->cold_blooded."<br>";
    
    echo "<br>";
    $kodok = new Frog("buduk",2,"yes");
    echo "Name : ".$kodok->name."<br>";
    echo "legs : ".$kodok->legs."<br>";
    echo "cold blooded : ".$kodok->cold_blooded."<br>";
    echo "Jump : ";
    $kodok->jump();
    echo "<br>";

    echo "<br>";
    $sungokong = new Ape("Kera Sakti",2,"yes");
    echo "Name : ".$sungokong->name."<br>";
    echo "legs : ".$sungokong->legs."<br>";
    echo "cold blooded : ".$sungokong->cold_blooded."<br>";
    echo "Yell : ";
    $sungokong->yell();
    echo "<br>";
?>